import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as firebase from 'firebase';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { main } from '@angular/compiler-cli/src/main';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient, private authService: AuthService, private router: Router) { }

  getUsers() {
    return this.httpClient.get(environment.url + `user/users`);
  }

  getUser(uid) {
    return this.httpClient.get(environment.url + `user/${uid}`).pipe(first());
  }

  createUser(role: string, mainEmail: string, secondEmail?: string) {
    if (secondEmail) {
      return new Observable(observer => {
        this.httpClient
          .post(environment.url + `user/createUser?type=${role}`, {
            email: mainEmail
          }).pipe(first()).subscribe(() => {
           this.httpClient
             .post(environment.url + `user/createUser?type=PARENT`, {
               email: secondEmail,
               studentEmail: mainEmail
             }).subscribe(() => {
               observer.next();
           });
        });
      });
    } else {
      return this.httpClient
        .post(environment.url + `user/createUser?type=${role}`,
          {
            email: mainEmail
          });
    }
  }

  createUserByLink(code, role, email, password, studentEmail?) {
    firebase.auth().signInWithEmailLink(email, window.location.href).then(data => {
      this.httpClient.post(environment.url + `user/createUserRole`, {
        uid: data.user.uid,
        role,
        studentEmail: studentEmail ? studentEmail : null
      }).pipe(first()).subscribe(() => {
        firebase.auth().currentUser.updatePassword(password).then(() => {
          this.authService.user = {...data.user, role};
          this.router.navigate(['app']);
        });
      });
    });
  }

  getUserIdByRole(role) {
    return this.httpClient.get(environment.url + `user/role/search/${role}`).pipe(first());
  }
}
