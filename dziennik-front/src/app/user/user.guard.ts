import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!firebase.auth().currentUser) {
        resolve(false);
        this.router.navigate(['login']);
      } else {
        resolve(true);
      }
    });
  }
}
