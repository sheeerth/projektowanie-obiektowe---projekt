import { BrowserModule } from '@angular/platform-browser';

/* Routing */
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

/* Angular Material */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

/* FormsModule */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Angular Flex Layout */
import { FlexLayoutModule } from "@angular/flex-layout";

/* Components */
import { LogInComponent } from './components/log-in/log-in.component';
import { CreateUserModalComponent, UiComponent } from './components/ui/ui.component';
import { AuthService } from './service/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { UserGuard } from './user/user.guard';
import { UserService } from './user/user.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { NewAccountComponent } from './components/new-account/new-account.component';
import { DegreesService } from './degrees/degrees.service';
import { PresentService } from './components/present/present.service';
import { BranchService } from './components/branch/branch.service';
import { CreateBranchComponent } from './components/ui/create-branch/create-branch.component';
import { AddDegreeComponent } from './components/ui/add-degree/add-degree.component';
import { AddLessonComponent } from './components/ui/add-lesson/add-lesson.component';


@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    LogInComponent,
    UiComponent,
    CreateUserModalComponent,
    NewAccountComponent,
    CreateBranchComponent,
    AddDegreeComponent,
    AddLessonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatCardModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
  ],
  entryComponents: [CreateUserModalComponent, CreateBranchComponent, AddDegreeComponent, AddLessonComponent],
  providers: [AuthService, UserGuard, UserService, DegreesService, PresentService, BranchService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }
