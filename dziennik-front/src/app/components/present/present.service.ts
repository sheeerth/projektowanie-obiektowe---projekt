import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PresentService {

  constructor(private httpClient: HttpClient) { }

  getUserPresentLIst(studentId) {
    return this.httpClient.get(environment.url + 'present-list/' + studentId).pipe(first());
  }

  createLesson(id, data) {
    return this.httpClient.post(environment.url + `lesson/${id}`, data).pipe(first());
  }

  getEmptyList(id) {
    return this.httpClient.get(environment.url + `present-list/empty/${id}`).pipe(first());
  }

  savePresentList(branchId, lessonId, data) {
    return this.httpClient.post(environment.url + `present-list/${branchId}/${lessonId}`, data).pipe(first());
  }
}
