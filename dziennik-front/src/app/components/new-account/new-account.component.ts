import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user/user.service';
import { AuthService } from '../../service/auth.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css']
})
export class NewAccountComponent implements OnInit {
  passwordForm: FormGroup;
  query: any;

  constructor(private authService: AuthService,
              private userService: UserService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.query = this.route.snapshot.queryParams;
    this.passwordForm = this.formBuilder.group({
      password: this.formBuilder.control(''),
    });
  }

  submit() {
    this.userService.createUserByLink(
      this.query.oobCode,
      this.query.accountType,
      this.query.email,
      this.passwordForm.getRawValue().password,
      this.query.studentEmail
    );
  }

}
