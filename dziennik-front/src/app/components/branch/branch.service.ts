import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BranchService {

  constructor(private httpClient: HttpClient) { }

  getBranchList() {
    return this.httpClient.get(environment.url + 'branch').pipe(first());
  }

  createBranch(data: any) {
    return this.httpClient.post(environment.url + `branch`, data).pipe(first());
  }
}
