import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  loginForm: FormGroup;
  wrongRequest: boolean = false;

  constructor(private router: Router, private authService: AuthService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: this.formBuilder.control('', []),
      password: this.formBuilder.control('', [])
    });
  }

  async loginClick() {
    this.wrongRequest = false;
    const formValue = this.loginForm.getRawValue();

    try {
      const user = await this.authService.signIn(formValue.email, formValue.password);
      this.authService.getRole(user.user.uid).pipe(first()).subscribe(async (data) => {
        this.authService.user = {...user.user, role: data.role, studentId: data.studentId ? data.studentId : null};
        this.router.navigate(['app']);
      });
    } catch (error) {
      this.wrongRequest = true;
      throw new Error(error);
    }
  }
}
