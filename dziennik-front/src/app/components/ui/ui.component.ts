import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { UserService } from '../../user/user.service';
import { finalize, first } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DegreesService } from '../../degrees/degrees.service';
import * as firebase from 'firebase';
import { PresentService } from '../present/present.service';
import { BranchService } from '../branch/branch.service';
import { CreateBranchComponent } from './create-branch/create-branch.component';
import { concat, Observable } from 'rxjs';
import { AddDegreeComponent } from './add-degree/add-degree.component';
import { AddLessonComponent } from './add-lesson/add-lesson.component';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.css']
})
export class UiComponent implements OnInit {
  userInterface: UserType;
  users: User[];
  degrees: any[];
  presentList: any[];
  branch: any[];
  isLoadingResults = true;
  isLoadingResultsSecond = true;
  displayedColumns: string[] = ['uid', 'email', 'role', 'action'];
  degreeDisplayColumn: string[] = ['subject', 'value'];
  presentDisplayColumn: string[] = ['subject', 'topic', 'present'];
  branchDisplayColumn: string[] = ['id', 'name', 'teacher', 'students'];
  branchTeachDisplayColumn: string[] = ['id', 'name', 'teacher', 'students', 'action'];
  resultsLength = 0;
  resultsLengthPresent = 0;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private authService: AuthService,
              private userService: UserService,
              private degreesService: DegreesService,
              private presentService: PresentService,
              private branchService: BranchService) { }

  ngOnInit() {
    switch (this.authService.user.role) {
      case UserType.admin:
        this.userInterface = UserType.admin;
        this.getUsers();
        this.getBranchList();
        break;
      case UserType.student:
        this.userInterface = UserType.student;
        this.getUserDegree(firebase.auth().currentUser.uid);
        this.getUserPresentList(firebase.auth().currentUser.uid);
        break;
      case UserType.parent:
        // @ts-ignore
        this.userInterface = UserType.parent;
        this.getUserDegree(this.authService.user.studentId);
        this.getUserPresentList(this.authService.user.studentId);
        break;
      case UserType.teacher:
        this.userInterface = UserType.teacher;
        this.getBranchList();
        this.getAllStudents();
        break;
    }
  }

  getUsers() {
    this.userService.getUsers().pipe(first()).subscribe((data: User[]) => {
      this.users = data;
      this.resultsLength = this.users.length;
      this.isLoadingResults = false;
    });
  }

  actionFabButton() {
    switch (this.userInterface) {
      case UserType.admin:
        this.createNewUserInit();
        break;
      case UserType.student:

        break;
      case UserType.parent:
        break;
      case UserType.teacher:
        break;
    }
  }

  createNewUserInit() {
    const dialogRef = this.dialog.open(CreateUserModalComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe((data) => this.createUser(data));
  }

  createUser({role, studentEmail, parentEmail, teacherEmail}) {
    switch (role) {
      case UserType.teacher:
        this.userService.createUser(role, teacherEmail).pipe(first()).subscribe();
        break;
      case UserType.student:
        this.userService.createUser(role, studentEmail, parentEmail).pipe(first()).subscribe();
        break;
    }
  }

  getUserDegree(uid: string) {
    this.degreesService.getStudentDegree(uid).subscribe((data: any[]) => {
      this.degrees = data;
      this.resultsLength = this.degrees.length;
      this.isLoadingResults = false;
    });
  }

  getUserPresentList(studentId) {
    this.presentService.getUserPresentLIst(studentId).subscribe((data: any[]) => {
      this.presentList = data;
      this.resultsLengthPresent = this.presentList.length;
      this.isLoadingResultsSecond = false;
    });
  }

  getBranchList() {
    this.branchService.getBranchList().pipe(finalize(() => {
      this.isLoadingResultsSecond = false;
    })).subscribe((data: any[]) => {
      this.branch = data;
      this.resultsLengthPresent = this.branch.length;
    });
  }

  createNewBranch() {
    const dialogRef = this.dialog.open(CreateBranchComponent, {width: '1000px'});

    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      this.branchService.createBranch(data).subscribe(response => console.log(response));
    });
  }

  getAllStudents() {
    this.userService.getUserIdByRole('STUDENT').subscribe((data: Array<string>) => {
      const requestArray: Array<Observable<any>> = [];
      const userArray = [];
      data.forEach(el => {
        requestArray.push(this.userService.getUser(el));
      });

      concat(...requestArray).pipe(finalize(() => {
        this.users = userArray;
        this.isLoadingResults  = false;
      })).subscribe(el => userArray.push(el));
    });
  }

  addDegree(uid: number) {
    const dialogRef = this.dialog.open(AddDegreeComponent, {width: '500px'});

    dialogRef.afterClosed().subscribe(data => {
      const body = {
        value: data.value,
        subject: data.subject,
        studentId: uid
      };

      this.degreesService.setStudentDegree(body).subscribe((response) => {
        console.log(response);
      });
    });
  }

  addLesson(id: any) {
    const dialogRef = this.dialog.open(AddLessonComponent, {
      width: '500px',
      data: {id}
    }, );

    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
    });
  }
}

export enum UserType {
  parent = 'PARENT',
  student = 'STUDENT',
  teacher = 'TEACHER',
  admin = 'ADMIN'
}

export interface User {
  uid: string;
  email: string;
  role: UserType;
}

@Component({
  selector: 'app-create-user-modal',
  templateUrl: 'create-user-template.html',
})
export class CreateUserModalComponent implements OnInit {
  choosenRole: string;
  roles = [
    {value: 'TEACHER', label: 'Nauczyciel'},
    {value: 'STUDENT', label: 'Uczeń'}
  ];
  teacherForm: FormGroup;
  studentForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CreateUserModalComponent>, private formBuilder: FormBuilder) {}

    ngOnInit(): void {
      this.teacherForm = this.formBuilder.group({
        teacherEmail: this.formBuilder.control('', [Validators.required, Validators.email])
      });

      this.studentForm = this.formBuilder.group({
        studentEmail: this.formBuilder.control('', [Validators.required, Validators.email]),
        parentEmail: this.formBuilder.control('', [Validators.required, Validators.email])
      });
    }

  onNoClick(): void {
    this.dialogRef.close({role: this.choosenRole, ...this.studentForm.getRawValue(), ...this.teacherForm.getRawValue()});
  }

}

