import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-degree',
  templateUrl: './add-degree.component.html',
  styleUrls: ['./add-degree.component.css']
})
export class AddDegreeComponent implements OnInit {
  value: FormControl;
  subject: FormControl;

  constructor(public dialogRef: MatDialogRef<AddDegreeComponent>, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.value = this.formBuilder.control('');
    this.subject = this.formBuilder.control('');
  }

  onNoClick(): void {
    this.dialogRef.close({value: this.value.value, subject: this.subject.value});
  }
}
