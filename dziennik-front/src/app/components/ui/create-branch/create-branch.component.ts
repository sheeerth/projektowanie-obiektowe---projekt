import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { UserService } from '../../../user/user.service';
import { concat, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-branch',
  templateUrl: './create-branch.component.html',
  styleUrls: ['./create-branch.component.css']
})
export class CreateBranchComponent implements OnInit {
  studentsArray: Array<any> = [];
  teacherArray: Array<any> = [];
  addTeacherArray: Array<any> = [];
  addStudentsArray: any = [];
  loading = false;
  name: FormControl;

  constructor(public dialogRef: MatDialogRef<CreateBranchComponent>,
              private userService: UserService,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.name = this.formBuilder.control('', [Validators.required]);

    this.loading = true;
    this.userService.getUserIdByRole('STUDENT').subscribe((data: Array<string>) => {
      const requestArray: Array<Observable<any>> = [];
      data.forEach(el => {
        requestArray.push(this.userService.getUser(el));
      });

      concat(...requestArray).pipe(finalize(() => {
        this.loading  = false;
      })).subscribe(el => this.studentsArray.push(el));
    });

    this.userService.getUserIdByRole('TEACHER').subscribe((data: Array<string>) => {
      const requestArray: Array<Observable<any>> = [];
      data.forEach(el => {
        requestArray.push(this.userService.getUser(el));
      });

      concat(...requestArray).pipe(finalize(() => {
        this.loading  = false;
      })).subscribe(el => this.teacherArray.push(el));
    });
  }

  onNoClick(): void {
    const studentsIds = this.addStudentsArray.map(el => el.uid);
    const teacherId = this.addTeacherArray[0].uid;


    this.dialogRef.close({name: this.name.value, studentsId: studentsIds, teacherId});
  }

  addStudent(id: any) {
    const findElement = this.studentsArray.findIndex(el => {
      return el.uid === id;
    });

    this.addStudentsArray.push(this.studentsArray[findElement]);
    this.studentsArray.splice(findElement, 1);
    console.log(id);
  }

  addTeacher(id: any) {
    const findElement = this.teacherArray.findIndex(el => {
      return el.uid === id;
    });

    this.addTeacherArray.push(this.teacherArray[findElement]);
    this.teacherArray.splice(findElement, 1);
    console.log(id);
  }
}
