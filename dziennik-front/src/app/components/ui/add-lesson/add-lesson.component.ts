import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PresentService } from '../../present/present.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { concat } from 'rxjs';
import { UserService } from '../../../user/user.service';
import { finalize } from 'rxjs/operators';
import { MatSelectionListChange } from '@angular/material/list';

class DialogData {
  id: string;
}

@Component({
  selector: 'app-add-lesson',
  templateUrl: './add-lesson.component.html',
  styleUrls: ['./add-lesson.component.css']
})
export class AddLessonComponent implements OnInit {
  topic: FormControl;
  subject: FormControl;
  secondView = false;
  list: any;
  studentsList: any = [];
  loading;
  lesson: any;

  constructor(public dialogRef: MatDialogRef<AddLessonComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private presentService: PresentService,
              private formBuilder: FormBuilder,
              private userService: UserService) {}

  ngOnInit() {
    this.topic = this.formBuilder.control('');
    this.subject = this.formBuilder.control('');
  }

  createLesson() {
    const body = {
      topic: this.topic.value,
      subject: this.subject.value,
    };
    this.presentService.createLesson(this.data.id, body).subscribe((lesson) => {
      this.secondView = true;
      this.loading = true;
      this.lesson = lesson;
      this.presentService.getEmptyList(this.data.id).subscribe((list: object) => {
        this.list = list;
        const userArray = [];
        // tslint:disable-next-line:forin
        for (const user in list) {
          userArray.push(user);
        }

        const requestUserArray = userArray.map(id => this.userService.getUser(id));

        concat(...requestUserArray).pipe(finalize(() => {
          this.loading = false;
        })).subscribe((data) => {
          this.studentsList.push(data);
        });
      });
    });
  }

  onNoClick() {
    this.presentService.savePresentList(this.data.id, this.lesson.id, this.list).subscribe(data => {
      this.dialogRef.close({data});
    });
  }

  changePresent(e: MatSelectionListChange) {
    this.list[e.option.value] = e.option.selected ? 1 : 0;
  }
}
