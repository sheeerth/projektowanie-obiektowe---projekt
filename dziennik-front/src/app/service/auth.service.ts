import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // tslint:disable-next-line:variable-name
  private _user: firebase.User  & {role: string,  studentId?: string};
  get user(): firebase.User  & { role: string, studentId?: string } {
    return this._user;
  }

  set user(value: firebase.User  & { role: string, studentId?: string}) {
    this._user = value;
  }

  authModule;

  constructor(private http: HttpClient, private router: Router) {
    const firebaseConfig = {
      apiKey: 'AIzaSyC9I8QcNU5GImhso8EBfBlIdCrVj3v62yI',
      authDomain: 'dziennik-elektroniczny-32597.firebaseapp.com',
      databaseURL: 'https://dziennik-elektroniczny-32597.firebaseio.com',
      projectId: 'dziennik-elektroniczny-32597',
      storageBucket: 'dziennik-elektroniczny-32597.appspot.com',
      messagingSenderId: '88118790116',
      appId: '1:88118790116:web:f3d10ae56411b61e87e9f1',
      measurementId: 'G-W2H6S4HCVN'
    };

    firebase.initializeApp(firebaseConfig);
  }

  async signIn(email, password) {
    const result = await firebase.auth().signInWithEmailAndPassword(email, password);

    const token = await firebase.auth().currentUser.getIdToken();

    return result;
  }

  getRole(uid: string): Observable<any> {
    return this.http.get(environment.url + `user/role/${uid}`);
  }

  signOut() {
    firebase.auth().signOut().then(() => {
      this.router.navigate(['login']);
    });
  }
}
