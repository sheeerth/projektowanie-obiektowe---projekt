import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DegreesService {

  constructor(private httpClient: HttpClient) { }

  getStudentDegree(uid: string) {
    return this.httpClient.get(environment.url + `degrees/${uid}`).pipe(first());
  }

  setStudentDegree(data) {
    return this.httpClient.post(environment.url + `degrees`, data).pipe(first());
  }
}
