import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import * as express from 'express';
import * as functions from 'firebase-functions';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const server = express();

export const createNestServer = async (expressInstance) => {
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(expressInstance),
  );

  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('Dziennik Elektroniczny')
    .setDescription('aplikacja przygotowana na potrzeby projektu ze studiów')
    .setVersion('1.0')
    .addServer('http://localhost:5000/dziennik-elektroniczny-32597/us-central1/api')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  return app.init();
};

createNestServer(server)
  .then(v => console.log('Nest Ready'))
  .catch(err => console.error('Nest broken', err));

export const api = functions.https.onRequest(server);
