import { Injectable } from '@nestjs/common';
import { AppService } from '../app.service';
import { BranchRequestData } from './branchRequestData';
import { UserService } from '../user/user.service';

@Injectable()
export class BranchService {
  constructor(private appService: AppService, private userService: UserService) { }

  async getBranch(id) {
    let result;
    const branchesList = await this.appService.databaseModule.collection('Branches').doc(id).get();

    result = {
      id,
      data: branchesList.data(),
    };

    return result;
  }

  async getBranches() {
    const branchesList = [];
    const branchesListDoc =  await this.appService.databaseModule.collection('Branches').get();
    const users = await this.userService.getUserByRole();

    branchesListDoc.forEach(doc => {
      branchesList.push({id: doc.id, data: doc.data()});
    });

    return branchesList.map(branch => {
      const user = users.find(u => u.uid === branch.data.teacherId);

      const students = branch.data.studentsId.map(id => users.find(u => u.uid === id));
      
      return {
        ...branch,
        teacher: user,
        students,
      };
    });
  }

  async createBranch({name, teacherId, studentsId}: BranchRequestData) {
    const documentReference = this.appService.databaseModule.collection('Branches').doc();

    try {
      await documentReference.set({
        name,
        teacherId,
        studentsId,
      });
    } catch (error) {
      console.log(error);
    }
  }

  async updateBranch(id: string, {name, teacherId, studentsId}: BranchRequestData) {
    const branch = await this.appService.databaseModule.collection('Branches').doc(id);

    return await branch.update({
      name,
      teacherId,
      studentsId,
    });
  }

  async deleteBranch(id: string) {
    return await this.appService.databaseModule.collection('Branches').doc(id).delete();
  }

  async getUserBranch(userId) {
    const result = await this.appService.databaseModule
      .collection('Branches').where('studentsId', 'array-contains', userId).get();

    let docId = null;
    result.forEach(el => {
      console.log(el.id);
      docId = el.id;
    });

    return docId;
  }
}
