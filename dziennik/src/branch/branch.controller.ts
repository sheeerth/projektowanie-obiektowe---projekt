import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';
import { BranchService } from './branch.service';
import { BranchRequestData } from './branchRequestData';
import { user } from 'firebase-functions/lib/providers/auth';

@ApiTags('Odziały')
@Controller('branch')
export class BranchController {
  constructor(private branchService: BranchService) { }

  @Get()
  async getBranchList() {
    try {
      return await this.branchService.getBranches();
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiParam({name: 'id'})
  @Get(':id')
  async getBranch(@Param('id') id: string) {
    try {
      return await this.branchService.getBranch(id);
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiParam({name: 'userId'})
  @Get('student/:userId')
  async getUserBranch(@Param('userId') userId: string) {
    try {
      console.log(userId);
      return await this.branchService.getUserBranch(userId);
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiBody({
    schema: {
      example: `{"name": "1a", "teacherId": "KruVpvOmlETkPWuPgn76OL50rPq1", "studentsId": ["8nOWlZwzaLRr0rB2l73lGjtyY8G2", "cE2c5XdYxtcGuOJaHvsstKXbnBj2"]}`,
    },
  })
  @Post()
  async createNewBranch(@Body() newBranch: BranchRequestData) {
    const data = {
      name: newBranch.name,
      teacherId: newBranch.teacherId,
      studentsId: newBranch.studentsId,
    };

    try {
      await this.branchService.createBranch(data);
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiParam({name: 'id'})
  @Delete(':id')
  async deleteExistBranch(@Param('id') id: string) {
    try {
      await this.branchService.deleteBranch(id);
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiParam({name: 'id'})
  @ApiBody({
    schema: {
      example: `{"name": "2a", "teacherId": "KruVpvOmlETkPWuPgn76OL50rPq1", "studentsId": ["8nOWlZwzaLRr0rB2l73lGjtyY8G2", "cE2c5XdYxtcGuOJaHvsstKXbnBj2"]}`,
    },
  })
  @Put(':id')
  async updateBranch(@Param('id') id: string, @Body() updateBranch: BranchRequestData) {
    try {
      return await this.branchService.updateBranch(id, updateBranch);
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
