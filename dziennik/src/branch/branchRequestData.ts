export interface BranchRequestData {
  name: string;
  teacherId: string;
  studentsId: string[];
}
