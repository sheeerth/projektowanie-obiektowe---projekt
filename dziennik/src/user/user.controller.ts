import { Body, Controller, Get, HttpException, HttpStatus, Param, Post, Query } from '@nestjs/common';
import { ApiBody, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { UserService } from './user.service';
import { UserType } from './user-type';

@ApiTags('Użytkownicy')
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @ApiQuery({name: 'idToken'})
  @Get('checkToken')
  async checkToken(@Query('idToken') idToken) {
    try {
      await this.userService.checkToken(idToken);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  @ApiQuery({name: 'type', enum: [UserType.student, UserType.parent, UserType.teacher]})
  @ApiBody({
    schema: {
      type: 'string',
      title: 'email',
      example: '{"email": "test@test.pl"}',
    },
  })
  @Post('createUser')
  async createUser(
    @Query('type') type: UserType = UserType.student,
    @Body('email') email: string,
    @Body('studentEmail') studentEmail?: string,
  ) {
    await this.userService.createUser(type, email, studentEmail);
  }

  @ApiBody({
    schema: {
      type: 'object',
      items: {
        type: 'string',
      },
      description: 'description',
      example: `{"uid": "%USER ID%", "role": "STUDENT"}`,
    },
  })
  @Post('createUserRole')
  async createUsersRole(@Body('uid') uid: string, @Body('role') role: UserType, @Body('studentEmail') studentEmail?: string) {
    try {
      await this.userService.saveUserRoleInDb(uid, role, studentEmail);
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('users')
  async getUsers() {
    try {
      return await this.userService.getUserByRole();
    } catch (error) {
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @ApiParam({name: 'uid'})
  @Get('role/:uid')
  async getUserRole(@Param('uid') uid) {
    return this.userService.getUserRole(uid);
  }

  @ApiParam({name: 'uid'})
  @Get(':uid')
  async getUser(@Param('uid') uid) {
    return this.userService.getUser(uid);
  }

  @ApiParam({name: 'role'})
  @Get('/role/search/:role')
  async getUserOneRole(@Param('role') role: UserType) {
    return this.userService.userOneRole(role);
  }
}
