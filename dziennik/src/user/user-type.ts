export enum UserType {
  parent = 'PARENT',
  student = 'STUDENT',
  teacher = 'TEACHER',
}
