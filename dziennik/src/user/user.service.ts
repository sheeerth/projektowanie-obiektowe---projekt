import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AppService } from '../app.service';
import { UserType } from './user-type';
import * as admin from 'firebase-admin';
import { EmailService } from '../email/email.service';

@Injectable()
export class UserService {
  constructor(private appService: AppService, private emailService: EmailService) {
  }

  async checkToken(idToken) {
    if (idToken) {
      try {
        return await this.appService.authModule.verifyIdToken(idToken);
      } catch (error) {
        throw new Error(error);
      }
    } else {
      throw new HttpException('Token isnt defined', HttpStatus.BAD_REQUEST);
    }
  }

  async createUser(type: UserType, email: string, studentEmail?: string) {
    let actionCodeSetting: admin.auth.ActionCodeSettings = null;

    if (studentEmail) {
      actionCodeSetting = {
        url: `http://localhost:4200/new?accountType=${type}&email=${email}&studentEmail=${studentEmail}`,
        // url: `https://dziennik-elektroniczny-32597.firebaseapp.com/new?accountType=${type}&email=${email}&studentEmail=${studentEmail}`,
        handleCodeInApp: true,
      };
    } else {
      actionCodeSetting = {
        url: `http://localhost:4200/new?accountType=${type}&email=${email}`,
        // url: `https://dziennik-elektroniczny-32597.firebaseapp.com/new?accountType=${type}&email=${email}`,
        handleCodeInApp: true,
      };
    }

    try {
      const link = await this.appService.authModule.generateSignInWithEmailLink(email, actionCodeSetting);

      await this.emailService.passwordLink(link, email);
    } catch (error) {
      throw new Error (error);
    }
  }

  async saveUserRoleInDb(uid: string, role: UserType, studentEmail?: string) {
    try {
      await this.appService.authModule.getUser(uid);
    } catch (error) {
      throw new Error(error);
    }

    if (studentEmail) {
      const result = await this.appService.authModule.getUserByEmail(studentEmail);
      return await this.appService.databaseModule.collection('Users').doc(uid).set({
        role,
        studentId: result.uid,
      });
    } else {
      return await this.appService.databaseModule.collection('Users').doc(uid).set({
        role,
      });
    }
  }

  async getUserByRole() {
    const result = await this.appService.authModule.listUsers();
    const userEntity = await this.appService.databaseModule.collection('Users').get();
    const userList = result.users;

    return userList.map(user => {
      let role;
      userEntity.docs.forEach(userRole => {
        if (userRole.id === user.uid) {
          role = userRole.data();
        }
      });

      return {
        ...user,
        ...role,
      };
    });
  }

  async getUser(uid) {
    const result = await this.appService.authModule.listUsers();
    const userEntity = await this.appService.databaseModule.collection('Users').doc(uid).get();
    const userList = result.users;

    const currentUser = userList.find(user => user.uid === uid);

    return {
      ...currentUser,
      ...userEntity.data(),
    };
  }

  async getUserRole(uid: string) {
    const result = await this.appService.databaseModule.collection('Users').doc(uid).get();

    return result.data();
  }

  async userOneRole(role: UserType) {
    const result =  await this.appService.databaseModule.collection('Users')
      .where('role', '==', role).get();

    console.log('test');
    const resultArray = [];
    console.log(result);

    result.forEach(e => {
      console.log(e.data())
      resultArray.push(e.id)
    });

    return resultArray;
  }
}
