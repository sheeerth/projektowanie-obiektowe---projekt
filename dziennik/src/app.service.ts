import { Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
// tslint:disable-next-line:no-var-requires
const serviceAccount = require('../dziennik-elektroniczny-32597-firebase-adminsdk-g86q6-7fffec947f.json');

// tslint:disable-next-line:no-var-requires

@Injectable()
export class AppService {
  // tslint:disable-next-line:variable-name
  private _authModule: admin.auth.Auth;
  get authModule(): admin.auth.Auth {
    return this._authModule;
  }

  // tslint:disable-next-line:variable-name
  private _databaseModule: FirebaseFirestore.Firestore;
  get databaseModule(): FirebaseFirestore.Firestore {
    return this._databaseModule;
  }

  constructor() {
    this.initializeFirebaseAdminSdk();
  }

  getHello(): string {
    return 'Hello World!';
  }

  initializeFirebaseAdminSdk() {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: 'https://dziennik-elektroniczny-32597.firebaseio.com',
    });

    this._authModule = admin.auth();
    this._databaseModule = admin.firestore();
  }
}
