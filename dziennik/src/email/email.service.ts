import { Injectable } from '@nestjs/common';
import * as sendGrid from '@sendgrid/mail';
const SENDGRID_API_KEY = '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';

@Injectable()
export class EmailService {
  constructor() {
    sendGrid.setApiKey(SENDGRID_API_KEY);
  }

  async passwordLink(link: string, email: string) {
    const msg = {
      to: email,
      from: 'kontakt@dziennik-elektroniczny.pl',
      subject: 'Zarejestruj się do Dziennik Elektroniczny',
      text: 'Poniżej twój link do zalogowania się:',
      html: `<a href="${link}">KLIKNIJ TUTAJ!!</a>`,
    };

    return await sendGrid.send(msg);
  }
}
