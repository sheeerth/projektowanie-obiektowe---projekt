import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';
import { Subject } from '../lesson/lesson.controller';
import { DegreesService } from './degrees.service';

@ApiTags('Oceny')
@Controller('degrees')
export class DegreesController {
  constructor(private degreeService: DegreesService) {
  }

  @ApiBody({
    schema: {
      example: '{"studentId":"8nOWlZwzaLRr0rB2l73lGjtyY8G2","value":"5","subject": 0}',
    },
  })
  @Post()
  async setNewDegree(@Body() requestData: setDegree) {
    return await this.degreeService.setDegree(requestData.studentId, requestData.value, requestData.subject);
  }

  @ApiParam({name: 'studentId', example: '8nOWlZwzaLRr0rB2l73lGjtyY8G2'})
  @Get(':studentId')
  async getStudentDegree(@Param('studentId') studentId: string) {
    return await this.degreeService.getDegree(studentId);
  }
}

interface setDegree {
  studentId: string;
  value: number;
  subject: Subject;
}
