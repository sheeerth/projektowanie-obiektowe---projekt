import { Injectable } from '@nestjs/common';
import { Subject } from '../lesson/lesson.controller';
import { AppService } from '../app.service';

@Injectable()
export class DegreesService {
  constructor(private appService: AppService) {
  }

  async setDegree(studentId: string, value: number, subject: Subject) {
    await this.appService.databaseModule
      .collection('Users').doc(studentId)
      .collection('Degrees').add({
        value,
        subject,
      });
  }

  async getDegree(studentId: string) {
    const data = await this.appService.databaseModule
      .collection('Users').doc(studentId)
      .collection('Degrees').get();

    const degreeArray = [];

    data.forEach(el => degreeArray.push(el.data()));

    return degreeArray;
  }
}
