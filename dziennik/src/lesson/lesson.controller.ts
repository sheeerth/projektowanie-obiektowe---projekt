import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';
import { LessonService } from './lesson.service';

@ApiTags('Lekcje')
@Controller('lesson')
export class LessonController {
  constructor(private lessonService: LessonService) {
  }

  @ApiParam({name: 'branchId'})
  @Get(':branchId')
  async getLessons(@Param('branchId') branchId: string) {
    return await this.lessonService.getlessonListForBranch(branchId);
  }

  @ApiParam({name: 'branchId'})
  @ApiParam({name: 'lessonId'})
  @Get(':branchId/:lessonId')
  async getLesson(@Param('branchId') branchId: string, @Param('lessonId') lessonId: string) {
    return await this.lessonService.getlessonForBranch(branchId, lessonId);
  }

  @ApiParam({name: 'branchId'})
  @ApiBody({
    schema: {
      example: '{"topic":"Przykładowy temat zajec", "subject": "0"}',
    },
  })
  @Post(':branchId')
  async createLesson(@Param('branchId') branchId: string, @Body() newLesson: lessonRequest) {
    const result =  await this.lessonService.createNewLessonForBranch(branchId, newLesson);

    return {id: result.id};
  }

  @ApiParam({name: 'branchId'})
  @ApiParam({name: 'lessonId'})
  @Delete(':branchId/:lessonId')
  async deleteLesson(@Param('branchId') branchId: string, @Param('lessonId') lessonId: string) {
    return await this.lessonService.deleteLessonForBranch(branchId, lessonId);
  }
}

// tslint:disable-next-line:class-name
export interface lessonRequest {
  topic: string;
  subject: Subject;
}

export enum Subject {
  'ENGLISH',
  'POLISH',
  'MATH',
}
