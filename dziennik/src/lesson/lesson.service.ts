import { Injectable } from '@nestjs/common';
import { AppService } from '../app.service';
import { lessonRequest, Subject } from './lesson.controller';

@Injectable()
export class LessonService {
  constructor(private appService: AppService) {
  }

  async createNewLessonForBranch(branchId: string, {topic, subject}: lessonRequest) {
    return await this.appService.databaseModule
      .collection('Branches')
      .doc(branchId)
      .collection('Lesson')
      .add({topic, subject});
  }

  async getlessonListForBranch(branchId: string) {
    const docListJson = [];
    const docList = await this.appService.databaseModule.collection('Branches').doc(branchId).collection('Lesson').get();

    docList.forEach(doc => {
      docListJson.push({id: doc.id, data: doc.data()});
    });

    return docListJson;
  }

  async getlessonForBranch(branchId: string, lessonId: string) {
    const doc = await this.appService.databaseModule
      .collection('Branches')
      .doc(branchId)
      .collection('Lesson')
      .doc(lessonId).get();

    return {
      id: lessonId,
      data: doc.data(),
    };
  }

  async deleteLessonForBranch(branchId: string, lessonId: string) {
    return await this.appService.databaseModule
      .collection('Branches')
      .doc(branchId)
      .collection('Lesson')
      .doc(lessonId)
      .delete();
  }
}
