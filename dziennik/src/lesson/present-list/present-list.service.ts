import { Injectable } from '@nestjs/common';
import { AppService } from '../../app.service';
import { user } from 'firebase-functions/lib/providers/auth';

@Injectable()
export class PresentListService {
  constructor(private appService: AppService) {
  }

  async createEmptyPresentListForLesson(branchId: string) {
    const branchElement = await this.appService.databaseModule.collection('Branches').doc(branchId).get();

    const studentsList: string[] = branchElement.data().studentsId;
    let presentList: {[key: string]: presentEnum} = {};

    studentsList.forEach(id => {
      const newStudent: {[key: string]: presentEnum} = {};
      newStudent[id] = presentEnum.absent;
      presentList = {...presentList, ...newStudent};
    });

    return presentList;
  }

  async createPresentListForLesson(branchId: string, lessonId: string, presentList: {[key: string]: presentEnum}) {
    await this.appService.databaseModule
      .collection('Branches').doc(branchId)
      .collection('Lesson').doc(lessonId)
      .update({present: presentList});
  }

  async getPresentListForLesson(branchId: string, lessonId: string) {
    const lessonInstance = await this.appService.databaseModule
      .collection('Branches').doc(branchId)
      .collection('Lesson').doc(lessonId)
      .get();

    return lessonInstance.data().present;
  }

  async getUserPresentList(userId) {
    const result = await this.appService.databaseModule
      .collection('Branches').where('studentsId', 'array-contains', userId).get();

    let docId;
    result.forEach(el => docId = el.id);

    const lessonCollection = await this.appService.databaseModule
      .collection('Branches').doc(docId)
      .collection('Lesson').select('present', 'topic', 'subject').get();

    const presentList = [];

    lessonCollection.forEach(el => presentList.push(el.data()));

    return presentList.map(el => {
      return {
        topic: el.topic,
        subject: el.subject,
        present : el.present[userId],
      };
    });
   }
}

export enum presentEnum {
  absent,
  present,
  late,
}
