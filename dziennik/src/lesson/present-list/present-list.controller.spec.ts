import { Test, TestingModule } from '@nestjs/testing';
import { PresentListController } from './present-list.controller';

describe('PresentList Controller', () => {
  let controller: PresentListController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PresentListController],
    }).compile();

    controller = module.get<PresentListController>(PresentListController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
