import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';
import { PresentListService } from './present-list.service';

@ApiTags('Listy Obecności')
@Controller('present-list')
export class PresentListController {
  constructor(private presentListService: PresentListService) {
  }

  @ApiParam({name: 'branchId'})
  @Get('empty/:branchId')
  async createEmptyList(@Param('branchId') branchId: string) {
    return await this.presentListService.createEmptyPresentListForLesson(branchId);
  }

  @ApiParam({name: 'branchId'})
  @ApiParam({name: 'lessonId'})
  @Get(':branchId/:lessonId')
  async getList(@Param('branchId') branchId: string, @Param('lessonId') lessonId: string) {
    return await this.presentListService.getPresentListForLesson(branchId, lessonId);
  }

  @ApiParam({name: 'branchId'})
  @ApiParam({name: 'lessonId'})
  @ApiBody({
    schema: {
      example: `{
        "8nOWlZwzaLRr0rB2l73lGjtyY8G2": 1,
        "cE2c5XdYxtcGuOJaHvsstKXbnBj2": 1
      }`,
    },
  })
  @Post(':branchId/:lessonId')
  async saveList(@Param('branchId') branchId: string, @Param('lessonId') lessonId: string, @Body() presentList) {
    await this.presentListService.createPresentListForLesson(branchId, lessonId, presentList);
  }

  @ApiParam({name: 'branchId'})
  @ApiParam({name: 'lessonId'})
  @ApiBody({
    schema: {
      example: `{
        "8nOWlZwzaLRr0rB2l73lGjtyY8G2": 1,
        "cE2c5XdYxtcGuOJaHvsstKXbnBj2": 2
      }`,
    },
  })
  @Put(':branchId/:lessonId')
  async updateList(@Param('branchId') branchId: string, @Param('lessonId') lessonId: string, @Body() presentList) {
    await this.presentListService.createPresentListForLesson(branchId, lessonId, presentList);
  }

  @ApiParam({name: 'studentId'})
  @Get(':studentId')
  async getPresentListForUser(@Param('studentId') studentId: string) {
    return await this.presentListService.getUserPresentList(studentId);
  }
}
