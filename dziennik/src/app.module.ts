import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './user/user.controller';
import { UserService } from './user/user.service';
import { EmailService } from './email/email.service';
import { BranchController } from './branch/branch.controller';
import { BranchService } from './branch/branch.service';
import { LessonController } from './lesson/lesson.controller';
import { LessonService } from './lesson/lesson.service';
import { PresentListController } from './lesson/present-list/present-list.controller';
import { PresentListService } from './lesson/present-list/present-list.service';
import { DegreesController } from './degrees/degrees.controller';
import { DegreesService } from './degrees/degrees.service';

@Module({
  imports: [],
  controllers: [AppController, UserController, BranchController, LessonController, PresentListController, DegreesController],
  providers: [AppService, UserService, EmailService, BranchService, LessonService, PresentListService, DegreesService],
})
export class AppModule {}
